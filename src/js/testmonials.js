"use strict"

const itemTesmonialsArray = [
{
    img: 'dist/img/testmonials/logo1.svg',
    alt: 'logo_SMASHING',
    description: `Sed vestibulum scelerisque urna, 
    eu finibus leo facilisis sit amet. Proin id dignissim magna. 
    Sed varius urna et pulvinar venenatis.`,
},
{
    img: 'dist/img/testmonials/logo2.svg',
    alt: 'logo_CODROPS',
    description: `Donec euismod dolor ut ultricies consequat. 
    Vivamus urna ipsum, rhoncus molestie neque ac, mollis eleifend nibh.`,
},
{
    img: 'dist/img/testmonials/logo3.svg',
    alt: 'logo_W',
    description: `In efficitur in velit et tempus. Duis nec odio dapibus, 
    suscipit erat fringilla, imperdiet nibh. Morbi tempus auctor felis ac vehicula.`,
},
{
    img: 'dist/img/testmonials/logo4.svg',
    alt: 'logo_Pixel_Buddha',
    description: 'Sed vestibulum scelerisque urna, eu finibus leo facilisis sit amet. Proin id dignissim magna. Sed varius urna et pulvinar venenatis.',
},
{
    img: 'dist/img/testmonials/logo5.svg',
    alt: 'logo_Creative_Bloq',
    description: `Praesent ut eros tristique, malesuada lectus vel, lobortis massa. 
    Nulla faucibus lorem id arcu consequat faucibus.`,
},
{
    img: 'dist/img/testmonials/logo6.svg',
    alt: 'logo_TNW',
    description: `Fusce pharetra erat id odio blandit,  nec pharetra eros venenatis. 
    Pellentesque porttitor cursus massa et vestibulum.`,
},
]

const gridItemsWrapper = document.querySelector('.testmonials__grid');
const gridItem = document.createElement('div');
const gridItemHTML = itemTesmonialsArray.map((el) => `
<div class="testmonials__grid-item">
						<img class="testmonials__item-img" src="${el.img}" alt="${el.alt}">
						<p class="testmonials__item-desc">${el.description}s.</p>

					</div>
`)

gridItemsWrapper.insertAdjacentHTML('beforeend', gridItemHTML.slice().join(''));

