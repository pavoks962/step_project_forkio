## Step-Project-Forkio
***
_Ця робота - підсумковий step-project з вивчення модулю Advanced HTML/CSS, що включав вивчення роботи npm, збірку проекту за допомогою gulp, застосування препроцесорів css._


### __У роботі було використано технології:__
***
* BEM - структура css файлів побудована за BEM-методологією;
* GULP - використано для збірки проекту;
* SCSS препроцесор;
* JS - dropdowm menu та список відгуків;
* HTML - розмітка сторінки;
* Node.js, npm.


### __Cклад учасників проекту:__
***
1. Ковтун Оксана (pavoks962@gmail.com).
2. Штендера Анна (ann.shtendera@gmail.com).

#### __Завдання студента №1 (Ковтун Оксана):__

* Зверстала навігаційне меню включно з випадаючим списком при малій роздільній здатності екрана.
* Зверстала секцію ___Dowload___.
* Зверстала секцію ___People Are Talking About Fork___.
* Оформила README.md

#### __Завдання студента №2 (Штендера Анна):__
 * Створила віддалений репозиторій на GitLab;
 * Налаштувала збірку проекту за допомогою Gulp;
* Зверстала блок ___Revolutionary Editor___.
* Зверстала секцію ___Here is what you get___.
* Зверстати секцію ___Fork Subscription Pricing___.
***

### __Запуск коду (script)__
***
1. gulp build
2. gulp dev

#### __Робота над проектом в команді:__
***
* створено у віддаленому репозиторії окрему branch develop для тестування та перевірки роботи коду покроково і по готовності.
* кожен виконавець працює над окремою секцією в окремо створеній гілці. 
* про готовність певної секції повідомляється напарника, створюється merge request into develop. Проводиться перевірка сумісності коду. Оновлюється локальна branch develop. Усуваються конфлікти.
* проводимо ревью коду один одного.
* в процесі роботи над сторінкою повідомляється напарник про введення стилів normalize, variables, @mixin.



[Проект можна переглянути](https://ann-shtendera.gitlab.io/step-project-forkio)

***